import socket
import RPi.GPIO as GPIO
import time
import datetime

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'Conexión finalizada!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)


def send(msg):
    message=msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))




#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
 
#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24
 
#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)
N = 1.5

def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
 
    # set Trigger after N seconds
    time.sleep(N)
    GPIO.output(GPIO_TRIGGER, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    dist = (TimeElapsed * 34300) / 2
    distance = dist / 100
    return distance
 
if __name__ == '__main__':
    try:
        while True:
            dist = distance()

            if float(dist) < 0.5:
                print ("Measured Distance = %.1f m" % dist)
                d = datetime.datetime.strptime(time.ctime(), "%a %b %d  %H:%M:%S %Y")
                DISTANCE = dist
                DAY = d.day
                MONTH = d.month
                YEAR = d.year
                HOUR = d.hour
                MINUTE = d.minute
                SECOND = d.second

                print(DISTANCE, DAY, MONTH, YEAR, HOUR, MINUTE)
                send('Distancia: '+ str(round(DISTANCE, 2)) + '\nDia: '+ str(DAY) + '\nMes: '+ str(MONTH) + '\nAño: '+ str(YEAR) + '\nHora: '+ str(HOUR) + '\nMinuto: '+ str(MINUTE) + '\nSegundo: ' + str(SECOND))

                

            time.sleep(N)
            
        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
        send(DISCONNECT_MESSAGE)






