#Libraries
import RPi.GPIO as GPIO
import time
import datetime
 
#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
 
#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24
 
#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)
N = 1.5


def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
 
    # set Trigger after N seconds
    time.sleep(N)
    GPIO.output(GPIO_TRIGGER, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    dist = (TimeElapsed * 34300) / 2
    distance = dist / 100
    return distance
 
if __name__ == '__main__':
    try:
        while True:
            dist = distance()

            if float(dist) > 0.5:
                print ("Measured Distance = %.1f m" % dist)
                d = datetime.datetime.strptime(time.ctime(), "%a %b %d  %H:%M:%S %Y")
                DISTANCE = dist
                DAY = d.day
                MONTH = d.month
                YEAR = d.year
                HOUR = d.hour
                MINUTE = d.minute

                print(DISTANCE, DAY, MONTH, YEAR, HOUR, MINUTE)
                

            time.sleep(N)
            
        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()